// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var db = null;

angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    //Esta línea debe comentarse, si no, se borraría la BBDD cada vez que se inicia la aplicación
    //$cordovaSQLite.deleteDB("my.db");

    db = $cordovaSQLite.openDB("my.db");
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS personas (id integer primary key, nombre text, apellido text, comentarios text)");
  });
})

.controller("ExampleController", function($scope, $cordovaSQLite) {
 
    $scope.insert = function(nombre, apellido, comentarios) {
        var query = "INSERT INTO personas (nombre, apellido, comentarios) VALUES (?,?,?)";
        $cordovaSQLite.execute(db, query, [nombre, apellido, comentarios]).then(function(res) {
            console.log("INSERT ID -> " + res.insertId);
            alert("Persona insertada en la BBDD");
        }, function (err) {
            console.error(err);
            alert(err);
        });
    }
 
    $scope.select = function(apellido) {
        var query = "SELECT nombre, apellido, comentarios FROM personas WHERE apellido = ?";
        $cordovaSQLite.execute(db, query, [apellido]).then(function(res) {
            if(res.rows.length > 0) {
                console.log("SELECTED -> " + res.rows.item(0).nombre + " " + res.rows.item(0).apellido + " " + res.rows.item(0).comentarios);
                alert("SELECTED -> " + res.rows.item(0).nombre + " " + res.rows.item(0).apellido + " " + res.rows.item(0).comentarios);
                $scope.resultado = res.rows.item(0).nombre + " " + res.rows.item(0).apellido + " " + res.rows.item(0).comentarios;
                
            } else {
                console.log("Ningún resultado");
                alert("Ningún resultado");
            }
        }, function (err) {
            console.error(err);
        });
    }
 
});
